#### Drupal version 8.6 update to 8.8.

***Upgrade Drupal 8.6 to 8.7 steps***
1. Backup .htaccess / robots.txt
1. Backup core - core/modules/views/src/Entity/Render/EntityTranslationRendererBase.php
1. composer.json and composer.lock change url
https://git.drupal.org/sandbox/onkeltem/2828817.git
to
https://git.drupalcode.org/sandbox/onkeltem-2828817.git
5. Update Drupal core 
```console
$ composer update drupal/core --with-dependencies
```
6. Security update plugin
```console
$ composer remove drupal/addtoany --no-update
$ composer require drupal/addtoany
$ composer remove drupal/smart_trim --no-update
$ composer require drupal/smart_trim
$ composer remove drupal/metatag --no-update
$ composer require drupal/metatag
$ composer remove drupal/field_group --no-update
$ composer require drupal/field_group:^3.0
$ composer remove drupal/admin_toolbar --no-update
$ composer require drupal/admin_toolbar:^2.0
```
7. Install Acquia Connector
```console
$ composer require drupal/acquia_search
```
8. Clear cache
```console
$ drush cr
```
9. Change backup file - .htaccess / robots.txt
10. Add old core function - preRenderByRelationship
```php
     /**
       * core/modules/views/src/Entity/Render/EntityTranslationRendererBase.php
       * 
       * Runs before each entity is rendered if a relationship is needed.
       *
       * @param \Drupal\views\ResultRow[] $result
       *   The full array of results from the query.
       * @param string $relationship
       *   The relationship to be used.
       */
      public function preRenderByRelationship(array $result, $relationship) {
        $view_builder = $this->view->rowPlugin->entityManager->getViewBuilder($this->entityType->id());
    
        foreach ($result as $row) {
          if ($entity = $this->getEntity($row, $relationship)) {
            $entity->view = $this->view;
            $this->build[$entity->id()] = $view_builder->view($entity, $this->view->rowPlugin->options['view_mode'], $this->getLangcodeByRelationship($row, $relationship));
          }
        }
      }
```
11. Add vcs section on composer.json


***Upgrade Drupal 8.7 to 8.8 steps***
1. Backup .htaccess / robots.txt
1. Backup core - core/modules/views/src/Entity/Render/EntityTranslationRendererBase.php
2. 先判斷升級至 Drupal 8.8停用或是需要更新的 plugin.
```console
$ composer prohibits drupal/core:^8.8
```
3. drupal/pathauto套件需要 >1.6
```console
$ composer remove drupal/pathauto --no-update
$ `composer require drupal/pathauto:^1.6`
```
4. Update composer/installers:^1.7
```console
$ composer remove composer/installers --no-update
$ composer update composer/installers:^1.7
$ composer requrire phpunit/phpunit-mock-objects:~2.3
```
5. 
```console
$ composer update drupal/core typo3/phar-stream-wrapper --dry-run
```
6. Add drupal-console-library on composer.json
```json
"extra": {
  "installer-paths": {
    "docroot/libraries/{$name}": ["type:drupal-library", "type:drupal-console-library"]
  },
}
```
7. Update to drupal/core:^8.8
```console
$ composer update drupal/core typo3/phar-stream-wrapper
```
8. Clear cache
```console
$ drush cr
```
9. Change backup file - .htaccess / robots.txt
10. Add old core function - preRenderByRelationship
```php
     /**
       * core/modules/views/src/Entity/Render/EntityTranslationRendererBase.php
       * 
       * Runs before each entity is rendered if a relationship is needed.
       *
       * @param \Drupal\views\ResultRow[] $result
       *   The full array of results from the query.
       * @param string $relationship
       *   The relationship to be used.
       */
      public function preRenderByRelationship(array $result, $relationship) {
        $view_builder = $this->view->rowPlugin->entityManager->getViewBuilder($this->entityType->id());
    
        foreach ($result as $row) {
          if ($entity = $this->getEntity($row, $relationship)) {
            $entity->view = $this->view;
            $this->build[$entity->id()] = $view_builder->view($entity, $this->view->rowPlugin->options['view_mode'], $this->getLangcodeByRelationship($row, $relationship));
          }
        }
      }
```
11. Add vcs section on composer.json


***Update available Drupal 8.8 modules***
1.Update Modules
```console
$ composer remove drupal/address --no-update
$ composer require drupal/address
$ composer remove drupal/hreflang --no-update
$ composer require drupal/hreflang
$ composer remove drupal/fontawesome --no-update
$ composer require drupal/fontawesome
$ composer require drupal/token:^1.6
$ composer remove drupal/webform --no-update
$ composer require drupal/webform:^5.8
```
