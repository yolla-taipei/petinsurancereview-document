##### PIR upgrade PHP 7.2 to PHP 7.3 
### Upgrade Drupal plugin
$ drush pmu acquia_connector acquia_search -y
$ composer remove drupal/acquia_search drupal/acquia_connector
$ composer require drupal/acquia_connector:^1.22
$ composer require drupal/acquia_search:^1.22
$ drush en acquia_connector -y
$ drush cr

############################# quote-v3-template & quote-v3-upgrade
### Upgrade Drupal core
$ composer update drupal/core

### Upgrade Drupal plugin
$ composer remove drupal/easy_breadcrumb --no-update
$ composer require drupal/easy_breadcrumb:^1.13
$ composer remove drupal/file_mdm drupal/file_mdm_exif drupal/image_effects
$ composer require drupal/file_mdm:^2.1 drupal/file_mdm_exif:^2.1 drupal/image_effects:^3.0
$ composer remove drupal/webform --no-update
$ composer require drupal/webform:^5.19