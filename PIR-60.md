#### First AMP Setting
1. drush en amp / amp_toolbar
2. Install Pet Insurance Review for AMP theme
3. Configuration/ AMP settings
    1. AMP theme : Pet Insurance Review for AMP
    2. Insurer enable
    3. Insurer configure : all region disabled
    4. Enabled Company info / Meta tags
4. Structure / Block layout / Custom block library
    1. Clone Footer Contact to Footer Contact AMP
5. Structure / Menus
    1. Clone Footer Menu to Footer Menu AMP
    2. Add link
6. Structure / Block layout / Pet Insurance Review for AMP
    1. Disabled
        1. Top Bar : Tabs 
        2. Footer : Newsletter Webform Launcher / Footer Contact
        3. CTA : Get a quote today
    2. Enabled 
        1. Footer : Footer Contact AMP
        2. Footer : Footer Menu AMP

#### Basic page AMP Setting
1. Basic page enable
    1. Basic page configure : all region disabled
    2. Enable Body / Meta tags
2. Edit Basic page : How it works
    1. Updated Box width="368px" and height="325px"
3. Edit Basic page : How to make a claim
    1. Updated Box width="368px" and height="325px"
4. Structure / Block layout / Custom block library
    1. Clone Homepage wide advert to Homepage wide advert AMP
    2. Block layout / Pet Insurance Review for AMP / Content
        1. Add Homepage wide advert - AMP and disabled
        2. Disabled Homepage wide advert and removed
5. Structure / Views / Insurer Logos
    1. Clone Block to Block AMP
    2. Block AMP Format : AMP Views Carousel
        1. Carousel type : carousel
        2. AMP Layout : fixed-height
        3. Height : 320
        4. Click Controls
    3. Structure / Block layout / Pet Insurance Review for AMP
        1. Accounts Place block 
            1. Add Insurer Logos: Block AMP and disabled
6. Structure / Views / Insurer Logos Canada
    1. Clone Block to Block AMP
    2. Block AMP Format : AMP Views Carousel
        1. Carousel type : carousel
        2. AMP Layout : fixed-height
        3. Height : 320
        4. Click Controls
    3. Structure / Block layout / Pet Insurance Review for AMP
        1. Accounts Place block 
            1. Add Insurer Logos Canada: Block AMP and disabled
7. Structure / Block layout / Pet Insurance Review for AMP
    1. Highlighted / Homepage Slider disabled
    2. Highlighted / Header CTA Buttons Mobile disabled
    2. Highlighted / Header CTA Buttons disabled
    
#### Blog AMP Setting
1. Blog enable
    1. Blog configure : all region disabled
    2. Enable Category / Body / Meta tags
2. Structure / Views / Related posts
    1. Clone Block to Block AMP
    2. Display name
        1. Administrative description : Related posts AMP
    3. Fields updated
        1. Content : Featured image
            1. Formatter : AMP Image Formatter
            2. AMP Layout : responsive
            3. Width : 315
            4. Height : 210
3. Structure / Block layout / Pet Insurance Review for AMP
    1. Page Bottom
        1. Add Blog: Related posts AMP
        2. Content types : Blog
        2. Disable Blog: Related posts
    2. Secondary
        1. Disable Share