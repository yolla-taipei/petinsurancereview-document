##### Get quote this year to calculate : sorting pet's born year 
```sql
SELECT
	pet_born_year,
	count(*) AS request_count 
FROM
	quote_request 
WHERE
	created >= '1577836802' 
	AND pet_species = 'dog' 
GROUP BY
	pet_born_year
HAVING
	count(*) >= 1 
ORDER BY
	request_count DESC
```

##### Get quote this year to calculate : sorting pet's breed
```sql
SELECT
	pet_breed,
	count(*) AS request_count 
FROM
	quote_request 
WHERE
	created >= '1577836802' 
	AND pet_species = 'dog' 
GROUP BY
	pet_breed
HAVING
	count(*) >= 1 
ORDER BY
	request_count DESC
``` 

##### Buy Now this year to calculate : leads sorting
```sql
SELECT
	r.api,
	qr.pet_breed,
	r.deductible,
	r.reimbursement,
	r.annual_limit,
	r.premium_period,
	count(*) AS count_insurer 
FROM
	leads AS l
	LEFT JOIN quote_response AS r ON l.quote_response = r.id
	LEFT JOIN quote_request AS qr ON r.quote_request = qr.id 
WHERE 
    qr.created >= '1577836802' 
	AND qr.pet_species = 'dog' 
	AND qr.pet_born_year = '2019'
	AND qr.pet_breed = 'Mixed Breed Medium (23 - 70lb when full grown)'
GROUP BY
	r.api,
	r.deductible,
	r.reimbursement,
	r.annual_limit,
	r.premium_period
HAVING
	count(*) >= 1 
ORDER BY
	count_insurer DESC,
	r.deductible DESC,
	r.reimbursement DESC,
	r.annual_limit DESC,
	r.premium_period DESC
```

##### This year to calculate : all request are pet born year 2019
```sql
SELECT
	qr.first_name,
	qr.last_name,
	qr.email,
	qr.pet_breed,
	qr.pet_born_year,
	qr.pet_species 
FROM
	quote_request AS qr 
WHERE
	qr.created >= '1577836802' 
	AND qr.pet_born_year = '2019' 
	AND qr.pet_species = 'dog' 
GROUP BY
	qr.email,
	qr.pet_breed
```

##### This year to calculate : recommend insurer premium
```sql
SELECT
	r.api,
	qr.address_zip,
	qr.pet_breed,
	r.deductible,
	r.reimbursement,
	r.annual_limit,
	r.premium_period,
	r.premium,
	count(*) AS count_insurer 
FROM
	leads AS l
	LEFT JOIN quote_response AS r ON l.quote_response = r.id
	LEFT JOIN quote_request AS qr ON r.quote_request = qr.id 
WHERE
	qr.created >= '1577836802' 
	AND qr.pet_species = 'dog' 
	AND qr.pet_born_year = '2019' 
	AND r.api = 'petsbest'
	AND r.deductible = '250'
	AND r.reimbursement = '90'
	AND r.annual_limit = '-1'
	AND r.premium_period = '1'
GROUP BY
	r.api,
	qr.pet_breed,
	qr.address_zip,
	r.deductible,
	r.reimbursement,
	r.annual_limit,
	r.premium_period,
	r.premium
HAVING
	count(*) >= 1 
ORDER BY
	count_insurer DESC
	LIMIT 0,1
```

