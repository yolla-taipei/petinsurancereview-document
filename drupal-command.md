### drush command
1. drush en {module-name} : install modules
2. drush dis {module-name} : disable modules (drush 8 not support)
3. drush pmu {module-name} : uninstall modules
4. drush vdis {views-name} : disable views
5. drush ven {views-name} : install views
6. drush sql-cli < ~/{sql-name} : import sql


### drupal 
1. print message
    1. drupal_set_message({string})