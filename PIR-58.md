### Trupanion-US-ID: 121  / Trupanion-Canada-ID: 46906 ###

***Select need fix data from Trupanion-Canada.***
```sql
SELECT
	nfd.nid AS nid,
	nfd.title AS title,
	nfd.`status` AS `status`,
	nfe.field_email_value AS email,
	DATE_FORMAT(FROM_UNIXTIME(nfd.created), '%Y-%m-%d %h:%m:%s') AS created,
	DATE_FORMAT(FROM_UNIXTIME(nfd.`changed`), '%Y-%m-%d %h:%m:%s') AS `changed`
FROM
	node_field_data AS nfd
	LEFT JOIN node__field_insurer AS nfi ON nfd.nid = nfi.entity_id 
	LEFT JOIN node__field_email AS nfe on nfd.nid = nfe.entity_id
WHERE
	nfd.type = 'review'
	AND nfi.field_insurer_target_id = '46906'
	AND nfe.field_email_value IS NULL
	AND nfd.`changed` > '1579637541'
	AND nfd.nid >= '772256'
ORDER BY nfd.`changed` DESC,
nfd.nid DESC
```

***Delete error data from Trupanion-Canada.***
```sql
DELETE nfd, nfi 
FROM
	node_field_data AS nfd
	LEFT JOIN node__field_insurer AS nfi ON nfd.nid = nfi.entity_id
	LEFT JOIN node__field_email AS nfe ON nfd.nid = nfe.entity_id 
WHERE
	nfd.type = 'review' 
	AND nfi.field_insurer_target_id = '46906' 
	AND nfe.field_email_value IS NULL 
	AND nfd.`changed` > '1579637541' 
	AND nfd.nid >= '772256'
```

***Updated published status.***
```sql
UPDATE node_field_data AS nfd
LEFT JOIN node__field_insurer AS nfi ON nfd.nid = nfi.entity_id
LEFT JOIN node__field_email AS nfe ON nfd.nid = nfe.entity_id 
SET nfd.`status` = 1 
WHERE
  nfd.type = 'review' 
  AND nfi.field_insurer_target_id = '46906' 
  AND nfe.field_email_value IS NULL 
  AND nfd.`changed` > '1579637541' 
  AND nfd.nid >= '772256' 
  AND nfd.`status` = 0
```