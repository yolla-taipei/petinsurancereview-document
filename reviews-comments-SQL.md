***Reviews SQL***
```sql
SELECT
	count(*)
FROM
	node__comment_node_review AS ncnr
	LEFT JOIN node_field_data AS nfd ON nfd.nid = ncnr.entity_id
WHERE
	ncnr.bundle = 'review' 
  AND nfd.`status` = 1
``` 
 
***Review Comments SQL***  
```sql
SELECT
	count(*) 
FROM
	comment_field_data AS cfd
	INNER JOIN node_field_data AS nfd ON cfd.entity_id = nfd.nid 
	LEFT JOIN node__field_insurer AS nfi ON nfd.nid = nfi.entity_id 
	LEFT JOIN comment__field_archived AS cfa ON cfd.cid = cfa.entity_id 
	LEFT JOIN node__field_archived AS nfa ON nfd.nid = nfa.entity_id 
WHERE
	cfd.comment_type = 'comment_node_review'
	AND cfd.entity_type = 'node'
	AND nfi.deleted = '0'
	AND cfd.`status` = 1
	AND nfd.`status` = 1
	AND ((
			cfa.field_archived_value = '0' 
			) 
	OR ( cfa.field_archived_value IS NULL )) 
	AND ((
			nfa.field_archived_value = '0' 
			) 
	OR ( nfa.field_archived_value IS NULL )) 
ORDER BY
	cfd.created DESC
``` 

