### generate pir p3 quotes
```log
drupal generate:module  \
--module="Quotes v3 Feature" \
--machine-name="pir_p3_quotes_feature" \
--module-path="/modules/custom" \
--description="Provides the v3 Get a quote system." \
--core="8.x" \
--package="Pet Insurance Review" \
--module-file --twigtemplate="no" --no-interaction
```

```log
drupal generate:module  \
--module="Quotes v3" \
--machine-name="pir_p3_quotes" \
--module-path="/modules/custom" \
--description="Provides the v3 Get a quote system." \
--core="8.x" \
--package="Pet Insurance Review" \
--module-file --twigtemplate="no" --no-interaction
```

### Disable Views
```log
$ drush vdis user_past_results
$ drush vdis quotes
$ drush vdis quotes_leads
$ drush vdis quote_requests
$ drush vdis quote_responses
$ drush vdis raw_quote_response
$ drush vdis raw_quote_responses
$ drush vdis breeds
$ drush cr
```

### Install pir p3 quotes module
```log
$ git merge quote branch
$ drush cr
$ drush pmu pir_p2_quotes_feature -y
$ drush en pir_p3_quotes -y
$ drush en pir_p2_quotes_feature -y
```

### Enable Views
```log
$ drush ven user_past_results
$ drush ven quotes
$ drush ven quotes_leads
$ drush ven quote_requests
$ drush ven quote_responses
$ drush ven raw_quote_response
$ drush ven raw_quote_responses
$ drush ven breeds
$ drush cr
```

### Uninstall pir p2 quotes module
```log
$ drush pmu pir_p2_quotes -y
```

### Create new table & new data - view_quotes_insurer_rating
```log
$ drush updb
$ drush view-quotes-insurer-sorting
```

### Acquia add new scheduled jobs
1. Every day at 00:30 UTC
```log

```

### Api Quotes : 
1. Api name : Quotes Api
2. Description : get all quotes data
3. Uri : api/quotes/{quoteID}

### Api Quotes filter : 
1. Api name : Quotes filter Api
2. Description : get all quotes filter data
3. Uri : api/quotes/filter/{quoteID}

### Api Insurer filter : 
1. Api name : Insurer filter Api
2. Description : get all insurer filter data
3. Uri : api/quote/{quoteID}/insurer/filter/{insurerID}

### Api get Quote : 
1. Api name : Quote Api
2. Description : get quote data
3. Uri : api/quote/{quoteID}/{insurerID}/{deductible}/{reimbursement}/{policyLimits}/{paymentCycle}

### Api get Response Plan : 
1. Api name : Response Plan API
2. Description : get response plan data
3. Uri : api/response/{responseID}

### Api get Dog or Cat breeds
1. Api name : Breeds API
2. Description : get dog or cat breeds data
3. Uri : api/breeds/{type}

### Api Quotes options : 
1. Api name : Quotes options Api
2. Description : get all quotes options data (with enable and disable)
3. Uri : api/quotes/{quoteID}/parameter/{insurerID}/{deductible}/{reimbursement}/{policyLimits}/{paymentCycle}

### Api Quotes insurer options : 
1. Api name : Quotes insurer options Api
2. Description : get all quotes by insurer options data (with enable and disable)
3. Uri : api/quotes/options/{quoteID}/{insurerID}

### Api Quote zip code exist : 
1. Api name : Quote zip code is exist
2. Description : user submit form zip code is exist
3. Uri : api/zip-exist/{code}

### Structure Block Configure
1. Block : Page title 
    1. hide pages
```log
/get-quote
/get-quote/*
/retrieving-quotes/*
/view-quotes*
/view-quotes/insurer*
/getting-buynow-link
/getting-policy
/v2-get-quote
/v2-get-quote/*
/v2-view-quotes/*
```
2. Block : Main navigation
    1. hide pages
```log
/getting-policy
```
3. Custom block library
    1. Title : Get Quote v3 Page Bottom insurers logo
    2. source
```html
<p><img alt="PetsBest" data-align="left" data-entity-type="" data-entity-uuid="" src="/sites/default/files/insurer-logo/PetsBest%20250x100.png" /><img alt="PetPlan" data-align="left" data-entity-type="" data-entity-uuid="" src="/sites/default/files/insurer-logo/Petplan%20PNG.png" /><img alt="HealthyPaws" data-align="left" data-entity-type="" data-entity-uuid="" src="/sites/default/files/insurer-logo/Healthy%20Paws%20Pet%20Insurance%20%26%20Foundation%20-%20Logo%20(4color)%20(1)_2.jpg" /><img alt="Trupanion-USA" data-align="left" data-entity-type="" data-entity-uuid="" src="/sites/default/files/insurer-logo/Trupanion%20USA%20250x100.png" /><img alt="Nationwide" data-align="left" data-entity-type="" data-entity-uuid="" src="/sites/default/files/insurer-logo/Nationwide%20250x100.png" /></p>
```
4. Block : Page Bottom add : Get Quote v3 Page Bottom insurers logo
    1. show pages : 
    2. don't display title
```log
/get-quote*
/get-quote/*
/get-quote/insuer/*
/retrieving-quotes/*
/view-quotes*
/view-quotes/insurer*
```

5. Block : Page Bottom add : Get Quote v3 Page Bottom Description
    1. show pages : 
    2. don't display title
```log
/get-quote*
/get-quote/*
/get-quote/insuer/*
/retrieving-quotes/*
/view-quotes*
/view-quotes/insurer*
```

6. Block : CTA
    1. Add Blog: Block Get Quote Footer
        1. show pages :
        2. don't display title
```log
/get-quote*
/get-quote/*
/get-quote/insuer/*
/retrieving-quotes/*
/view-quotes*
/view-quotes/insurer*
```
    2. Footer Advert
        1. hide pages : 
```log
/get-quote*
/get-quote/*
/get-quote/insuer/*
/retrieving-quotes/*
/view-quotes*
/view-quotes/insurer*
```

7. GetQuote page AB test
```log
$ drush en abtestui
```