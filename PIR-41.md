#### Spot Pet Insurance get price step.

__Creating-a-quote, then get quote id.__
``https://api.spotpetins.com/qa/quoting/quotes``
```json
{
    "billingAddress": {
        "street1": "123 Sesame Street",
        "street2": "Apt A",
        "city": "Manhattan",
        "state": "New York",
        "zipcode": "10001"
    },
    "ratingAddress": {
        "street1": "123 Sesame Street",
        "street2": "Apt A",
        "city": "Manhattan",
        "state": "New York",
        "zipcode": "10001"
    },
    "firstName": "Chen",
    "lastName": "Jeri",
    "emailAddress": "jeri.chen0805@yahoo.com.tw",
    "goPaperless":true,
    "brand": "APHI",
    "allowMarketing": false,
    "effectiveDate": "2020-08-01"
}
```
__Creating-a-pet, then get pet id.__
``https://api.spotpetins.com/qa/quoting/quotes/{quoteId}/pets``
```json
{
    "name": "Sparky",
    "breed": {
        "species": "Dog",
        "type": "PureBreed",
        "name": "Saint Bernard"
    },
    "birthDate": "2011-07-18",
    "gender": "M",
    "isSpayedOrNeutered": true
}
```
__Get price by quote id and pet id.__
``https://api.spotpetins.com/qa/quoting/quotes/{quoteId}/pets/{petId}/options``
```console
https://api.spotpetins.com/qa/quoting/quotes/96d5f93c-e443-ea11-80ea-0050569d4532/pets/10f12945-e443-ea11-80ea-0050569d4532/options
```
