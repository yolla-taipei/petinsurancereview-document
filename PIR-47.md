***Insurance provided by insurance pet breeds.***
```sql
SELECT
    species,
    breed,
    i_company AS insurer_name 
FROM
    breed_codes 
WHERE
    species = 'dog' 
ORDER BY
    i_company ASC,
    species ASC,
    breed ASC
```