```sql
SELECT 'users' as users, count(*) FROM users
UNION
SELECT 'users_field_data' as users_field_data, count(*) FROM users_field_data
UNION
SELECT 'user__field_first_name' as user__field_first_name, count(*) FROM user__field_first_name
UNION
SELECT 'user__field_last_name' as user__field_last_name, count(*) FROM user__field_last_name
UNION
SELECT 'quote_request' as quote_request, count(*) FROM quote_request
UNION
SELECT 'quote_response' as quote_response, count(*) FROM quote_response
```

```sql
=============================== users =============================
SELECT 
	u.uid AS uid,
	u.uuid AS uuid,
	ufd.`name` AS `name`,
	ufd.mail AS email,
	uffn.field_first_name_value AS first_name,
	ufln.field_last_name_value AS last_name,
	ufz.field_zip_value AS zip,
	ufas.field_address_state_value AS state,
	ufc.field_city_value AS city,
	ufal1.field_address_line_1_value AS address1,
	ufal2.field_address_line_2_value AS address2,
	ufrn.field_receive_newsletter_value AS is_subscribe_newsletter
FROM  users AS u
LEFT JOIN users_field_data AS ufd ON u.uid = ufd.uid
LEFT JOIN user__field_first_name AS uffn ON u.uid = uffn.entity_id
LEFT JOIN user__field_last_name AS ufln ON u.uid = ufln.entity_id
LEFT JOIN user__field_zip AS ufz ON u.uid = ufz.entity_id
LEFT JOIN user__field_address_state AS ufas ON u.uid = ufas.entity_id
LEFT JOIN user__field_city AS ufc ON u.uid = ufc.entity_id
LEFT JOIN user__field_address_line_1 AS ufal1 ON u.uid = ufal1.entity_id
LEFT JOIN user__field_address_line_2 AS ufal2 ON u.uid = ufal2.entity_id
LIMIT 10
WHERE ufz.field_zip_value IS NOT NULL
AND ufas.field_address_state_value IS NOT NULL
AND ufc.field_city_value IS NOT NULL
AND ufal1.field_address_line_1_value IS NOT NULL
AND ufal2.field_address_line_2_value IS NOT NULL
```

```sql
=============================== quote =============================
SELECT 
	u.uid AS uid,
	u.uuid AS uuid,
	qreq.pet_name AS pet_name,
	qreq.pet_species AS pet_species,
	qreq.pet_breed AS pet_breed,
	qreq.pet_gender AS pet_gender,
	qreq.pet_born_month AS pet_born_month,
	qreq.pet_born_year AS pet_born_year,
	qreq.pet_spayed AS pet_spayed,
	qreq.first_name AS first_name,
	qreq.last_name AS last_name,
	qreq.address_zip AS zip,
	qreq.address_state AS state,
	qreq.address_city AS city,
	qreq.address_line1 AS address_line1,
	qreq.address_line2 AS address_line2,
	qreq.email AS email
FROM  users AS u
LEFT JOIN quote_request AS qreq ON u.uid = qreq.user__target_id
LEFT JOIN quote_response AS qres ON qreq.id = qres.quote_request
LIMIT 10
```

```sql
=============================== click =============================
SELECT 
	u.uid AS uid,
	u.uuid AS uuid,
	c.ip AS ip,
	DATE_FORMAT(FROM_UNIXTIME(c.created), '%Y-%m-%d %h:%m:%s') AS click_created,
	ufd.`name` AS `name`,
	ufd.mail AS email,
	uffn.field_first_name_value AS first_name,
	ufln.field_last_name_value AS last_name
FROM click AS c
LEFT JOIN users AS u ON c.user_id = u.uid
LEFT JOIN users_field_data AS ufd ON u.uid = ufd.uid
LEFT JOIN user__field_first_name AS uffn ON u.uid = uffn.entity_id
LEFT JOIN user__field_last_name AS ufln ON u.uid = ufln.entity_id
GROUP BY c.ip
ORDER BY c.created DESC
LIMIT 100
```

```sql
=============================== buy click =============================
SELECT 
	l.user__target_id AS uid,
	l.quote_request AS quote_request_id,
	l.insurer AS insurer,
	DATE_FORMAT(FROM_UNIXTIME(l.created), '%Y-%m-%d %h:%m:%s') AS click_buy_created,
	l.user_agent AS user_agent,
	qreq.pet_name AS pet_name,
	qreq.pet_species AS pet_species,
	qreq.pet_breed AS pet_breed,
	qreq.pet_gender AS pet_gender,
	qreq.pet_born_month AS pet_born_month,
	qreq.pet_born_year AS pet_born_year,
	qreq.pet_spayed AS pet_spayed,
	qreq.first_name AS first_name,
	qreq.last_name AS last_name,
	qreq.email AS email
FROM  leads AS l
LEFT JOIN quote_request AS qreq ON l.quote_request = qreq.id
ORDER BY l.created DESC
```

```sql
============================ reviews ==============================
SELECT 
	ncnr.entity_id AS id,
	ces.cid AS cid,
	ces.last_comment_uid AS uid,
	nfn.field_nickname_value AS user_name,
	nfe.field_email_value AS email,
	nfs.field_state_value AS state_key,
	nfd.title AS title,
	nb.body_value AS body,
	nfd.`status` AS `status`,
	nfi.field_insurer_target_id AS insurer_id,
	nfcb.field_cat_breed_value AS cat_breed,
	nfdb.field_dog_breed_value AS dog_breed,
	nfa.field_age_value AS pet_age,
	nfr.field_recommend_value AS is_recommend,
	nfu.field_usepir_value AS is_helpful,
	DATE_FORMAT(FROM_UNIXTIME(nfd.created), '%Y-%m-%d %h:%m:%s') AS review_created_at,
	DATE_FORMAT(FROM_UNIXTIME(nfd.`changed`), '%Y-%m-%d %h:%m:%s') AS review_updated_at,
	ces.comment_count AS comment_count
FROM node__comment_node_review AS ncnr
LEFT JOIN comment_entity_statistics AS ces ON ces.entity_id = ncnr.entity_id
LEFT JOIN node__field_nickname AS nfn ON ncnr.entity_id = nfn.entity_id
LEFT JOIN node__field_email AS nfe ON ncnr.entity_id = nfe.entity_id
LEFT JOIN node__field_state AS nfs ON ncnr.entity_id = nfs.entity_id
LEFT JOIN node_field_data AS nfd ON nfd.nid = ncnr.entity_id
LEFT JOIN node__body AS nb ON ncnr.entity_id = nb.entity_id
LEFT JOIN node__field_insurer AS nfi ON ncnr.entity_id = nfi.entity_id
LEFT JOIN node__field_cat_breed AS nfcb ON ncnr.entity_id = nfcb.entity_id
LEFT JOIN node__field_dog_breed AS nfdb ON ncnr.entity_id = nfdb.entity_id
LEFT JOIN node__field_age AS nfa ON ncnr.entity_id = nfa.entity_id
LEFT JOIN node__field_recommend AS nfr ON ncnr.entity_id = nfr.entity_id
LEFT JOIN node__field_usepir AS nfu ON ncnr.entity_id = nfu.entity_id
WHERE ncnr.bundle = 'review'
AND ncnr.entity_id = '754566'
ORDER BY ces.last_comment_timestamp DESC
LIMIT 10
```

```sql
============================ comment ==============================
SELECT 
	c.cid AS cid,
	cfd.uid AS uid,
	cfd.field_name AS `name`,
	cfe.field_email_value AS email,
	cfd.subject AS body,
	ccb.comment_body_value AS comment_body,
	nfs.field_state_value AS state,
	ufpt.field_pet_types_value AS pet,
	cfd.entity_id AS review_id,
	cfd.`status` AS `status`,
	DATE_FORMAT(FROM_UNIXTIME(cfd.created), '%Y-%m-%d %h:%m:%s') AS created_at,
	cfd.hostname AS ip
FROM `comment` AS c
LEFT JOIN comment_field_data AS cfd ON c.cid = cfd.cid
LEFT JOIN comment__field_email AS cfe ON c.cid = cfe.entity_id
LEFT JOIN comment__comment_body AS ccb ON c.cid = ccb.entity_id
LEFT JOIN node__field_state AS nfs ON cfd.uid = nfs.entity_id
LEFT JOIN user__field_pet_types AS ufpt ON cfd.uid = ufpt.entity_id
WHERE c.comment_type = 'comment_node_review'
-- AND c.cid = '12621'
AND cfd.entity_id = '734131'
```

```sql
============================ Overall_rating(reviews rating) ==============================
SELECT 
	nfi.field_insurer_target_id AS insurer_id,
	nfd.title AS insurer_name,
	nfn.entity_id AS uid,
	nfn.field_nickname_value AS user_name,
	nfe.field_email_value AS email,
	nfr.field_rating_value AS overall_rating
FROM node__field_rating AS nfr
LEFT JOIN node__comment_node_review AS ncnr ON ncnr.entity_id = nfr.entity_id
LEFT JOIN node__field_nickname AS nfn ON ncnr.entity_id = nfn.entity_id
LEFT JOIN node__field_email AS nfe ON ncnr.entity_id = nfe.entity_id
LEFT JOIN node__field_insurer AS nfi ON nfr.entity_id = nfi.entity_id
LEFT JOIN node_field_data AS nfd ON nfd.nid = nfi.field_insurer_target_id
WHERE nfr.bundle = 'review'
ORDER BY nfi.field_insurer_target_id DESC
LIMIT 100
```

```sql
============================ Customer_service_rating ==============================
SELECT 
	nfi.field_insurer_target_id AS insurer_id,
	nfd.title AS insurer_name,
	nfn.entity_id AS uid,
	nfn.field_nickname_value AS user_name,
	nfe.field_email_value AS email,
	nfcr.field_customer_rating_value AS customer_rating
FROM node__field_customer_rating AS nfcr
LEFT JOIN node__comment_node_review AS ncnr ON ncnr.entity_id = nfcr.entity_id
LEFT JOIN node__field_nickname AS nfn ON ncnr.entity_id = nfn.entity_id
LEFT JOIN node__field_email AS nfe ON ncnr.entity_id = nfe.entity_id
LEFT JOIN node__field_insurer AS nfi ON nfcr.entity_id = nfi.entity_id
LEFT JOIN node_field_data AS nfd ON nfd.nid = nfi.field_insurer_target_id
WHERE nfcr.bundle = 'review'
ORDER BY nfi.field_insurer_target_id DESC
LIMIT 100
```

```sql
============================ Claims_rating ==============================
SELECT 
	nfi.field_insurer_target_id AS insurer_id,
	nfd.title AS insurer_name,
	nfn.entity_id AS uid,
	nfn.field_nickname_value AS user_name,
	nfe.field_email_value AS email,
	nfcr.field_claims_rating_value AS clamins_rating
FROM node__field_claims_rating AS nfcr
LEFT JOIN node__comment_node_review AS ncnr ON ncnr.entity_id = nfcr.entity_id
LEFT JOIN node__field_nickname AS nfn ON ncnr.entity_id = nfn.entity_id
LEFT JOIN node__field_email AS nfe ON ncnr.entity_id = nfe.entity_id
LEFT JOIN node__field_insurer AS nfi ON nfcr.entity_id = nfi.entity_id
LEFT JOIN node_field_data AS nfd ON nfd.nid = nfi.field_insurer_target_id
WHERE nfcr.bundle = 'review'
ORDER BY nfi.field_insurer_target_id DESC
LIMIT 100
```

```sql
======================== Contents - single page ======================
SELECT
	nfd.nid AS id,
	nfd.type AS type,
	nfd.title AS title,
	nb.body_value AS body,
	nfhi.field_header_image_alt AS image_alt,
	fm.filename AS filename,
	fm.uri AS file_url,
	nfd.`status` AS `status`,
	nfd.uid AS author_uid,
	ufd.`name` AS author_name,
	DATE_FORMAT( FROM_UNIXTIME( nfd.created ), '%Y-%m-%d %h:%m:%s' ) AS created_at,
	DATE_FORMAT( FROM_UNIXTIME( nfd.`changed` ), '%Y-%m-%d %h:%m:%s' ) AS updated_at 
FROM node_field_data AS nfd
LEFT JOIN users_field_data AS ufd ON nfd.uid = ufd.uid
LEFT JOIN node__body AS nb ON nfd.nid = nb.entity_id 
LEFT JOIN node__field_header_image AS nfhi ON nfd.nid = nfhi.entity_id
LEFT JOIN file_managed AS fm ON nfhi.field_header_image_target_id = fm.fid
WHERE nfd.type = 'page' 
ORDER BY nfd.`changed`
LIMIT 10
```

```sql
======================= Contents - breed guide ===========================
SELECT
	nfd.nid AS id,
	nfd.type AS type,
	nfd.title AS title,
	nb.body_value AS body,
	nfhi.field_header_image_alt AS image_alt,
	fm.filename AS filename,
	fm.uri AS file_url,
	nfd.`status` AS `status`,
	nfd.uid AS author_uid,
	ufd.`name` AS author_name,
	DATE_FORMAT( FROM_UNIXTIME( nfd.created ), '%Y-%m-%d %h:%m:%s' ) AS created_at,
	DATE_FORMAT( FROM_UNIXTIME( nfd.`changed` ), '%Y-%m-%d %h:%m:%s' ) AS updated_at 
FROM
	node_field_data AS nfd
	LEFT JOIN users_field_data AS ufd ON nfd.uid = ufd.uid
	LEFT JOIN node__body AS nb ON nfd.nid = nb.entity_id 
	LEFT JOIN node__field_header_image AS nfhi ON nfd.nid = nfhi.entity_id
	LEFT JOIN file_managed AS fm ON nfhi.field_header_image_target_id = fm.fid
WHERE
	nfd.type = 'breeds' 
ORDER BY
	nfd.`changed`
LIMIT 10
```

```sql
======================= Contents - owners-area(dog or cat) ===========================
SELECT
	nfd.nid AS id,
	nfd.type AS type,
	nfc.field_category_target_id AS category_id,
	ttfd.`name` AS category_name,
	nfd.title AS title,
	nb.body_value AS body,
	nfhi.field_header_image_alt AS image_alt,
	fm.filename AS filename,
	fm.uri AS file_url,
	nfd.`status` AS `status`,
	nfd.uid AS author_uid,
	ufd.`name` AS author_name,
	DATE_FORMAT( FROM_UNIXTIME( nfd.created ), '%Y-%m-%d %h:%m:%s' ) AS created_at,
	DATE_FORMAT( FROM_UNIXTIME( nfd.`changed` ), '%Y-%m-%d %h:%m:%s' ) AS updated_at 
FROM
	node_field_data AS nfd
	LEFT JOIN users_field_data AS ufd ON nfd.uid = ufd.uid
	LEFT JOIN node__body AS nb ON nfd.nid = nb.entity_id 
	LEFT JOIN node__field_header_image AS nfhi ON nfd.nid = nfhi.entity_id
	LEFT JOIN file_managed AS fm ON nfhi.field_header_image_target_id = fm.fid
	LEFT JOIN node__field_category AS nfc ON nfd.nid = nfc.entity_id
	LEFT JOIN taxonomy_term_field_data AS ttfd ON nfc.field_category_target_id = ttfd.tid
	LEFT JOIN taxonomy_term__parent AS ttp ON ttfd.tid = ttp.entity_id
WHERE
	nfd.type = 'owners_area' 
AND ttp.parent_target_id = 22
OR ttp.parent_target_id = 23
ORDER BY
	nfd.`changed`
LIMIT 10
```

```sql
======================= Contents - blog ===========================
SELECT
	nfd.nid AS id,
	nfd.type AS type,
	nfc.field_category_target_id AS category_id,
	ttfd.`name` AS category_name,
	nfd.title AS title,
	nb.body_value AS body,
	nfhi.field_header_image_alt AS image_alt,
	fm.filename AS filename,
	fm.uri AS file_url,
	nfd.`status` AS `status`,
	nfd.uid AS author_uid,
	ufd.`name` AS author_name,
	DATE_FORMAT( FROM_UNIXTIME( nfd.created ), '%Y-%m-%d %h:%m:%s' ) AS created_at,
	DATE_FORMAT( FROM_UNIXTIME( nfd.`changed` ), '%Y-%m-%d %h:%m:%s' ) AS updated_at 
FROM
	node_field_data AS nfd
	LEFT JOIN users_field_data AS ufd ON nfd.uid = ufd.uid
	LEFT JOIN node__body AS nb ON nfd.nid = nb.entity_id 
	LEFT JOIN node__field_header_image AS nfhi ON nfd.nid = nfhi.entity_id
	LEFT JOIN file_managed AS fm ON nfhi.field_header_image_target_id = fm.fid
	LEFT JOIN node__field_category AS nfc ON nfd.nid = nfc.entity_id
	LEFT JOIN taxonomy_term_field_data AS ttfd ON nfc.field_category_target_id = ttfd.tid
	LEFT JOIN taxonomy_term__parent AS ttp ON ttfd.tid = ttp.entity_id
WHERE
	nfd.type = 'blog' 
ORDER BY
	nfd.nid DESC
LIMIT 10
```

```sql
=============================== 排除相同的寵物名跟user ============================
SELECT
	qreq.email AS email,
	qreq.pet_name AS pet_name,
	qreq.pet_species AS pet_species,
	qreq.pet_breed AS pet_breed,
	qreq.pet_gender AS pet_gender,
	qreq.pet_born_month AS pet_born_month,
	qreq.pet_born_year AS pet_born_year,
	qreq.pet_spayed AS pet_spayed,
	qreq.first_name AS first_name,
	qreq.last_name AS last_name 
FROM
	quote_request AS qreq
	LEFT JOIN users AS u ON qreq.user__target_id = u.uid
	LEFT JOIN user__roles AS ur ON u.uid = ur.entity_id 
WHERE
	qreq.email IS NOT NULL 
	AND qreq.first_name IS NOT NULL 
	AND qreq.last_name IS NOT NULL 
	AND qreq.pet_name IS NOT NULL 
	AND ur.roles_target_id = 'shadow' 
GROUP BY
	u.uid,
	qreq.email,
	qreq.pet_name,
	qreq.pet_species,
	qreq.pet_breed,
	qreq.pet_gender,
	qreq.pet_born_month,
	qreq.pet_born_year,
	qreq.pet_spayed,
	qreq.first_name,
	qreq.last_name
ORDER BY
	u.uid DESC 
	LIMIT 10
```

======================== Menus ===============================
```sql
SELECT
	id,
	title,
	description,
	menu_name,
	link__uri,
	weight,
	parent,
	enabled,
	DATE_FORMAT( FROM_UNIXTIME( `changed` ), '%Y-%m-%d %h:%m:%s' ) AS updated_at 
FROM
	menu_link_content_data 
ORDER BY
	weight 
	LIMIT 10
```

```sql
======================= insurer review rating total count ===================
SELECT
	nfi.field_insurer_target_id AS insurer_id,
	count(*) AS review_count,
	SUM(nfr.field_rating_value) AS rating_total
FROM
	node_field_data AS nfd
	RIGHT JOIN node__field_insurer AS nfi ON nfi.entity_id = nfd.nid 
	RIGHT JOIN node__field_rating AS nfr ON nfr.entity_id = nfd.nid
WHERE
	nfd.title IS NOT NULL 
	AND nfd.`status` = 1 
	AND nfd.type = 'review'
GROUP BY nfi.field_insurer_target_id
ORDER BY nfi.field_insurer_target_id
```

```sql
======================= click type entities ===================
SELECT
	id,
	`name`,
	insurer,
	click_path,
	DATE_FORMAT( FROM_UNIXTIME( `changed` ), '%Y-%m-%d %h:%m:%s' ) AS updated_at 
FROM
	click_type 
ORDER BY
	`name` ASC
```

```sql
======================= change nationwide plan id ===================
SELECT
	id,
	revision_id,
	type,
	uid,
	parent_id,
	parent_type,
	parent_field_name,
	DATE_FORMAT( FROM_UNIXTIME( created ), '%Y-%m-%d %h:%m:%s' ) AS created_at 
FROM
	paragraphs_item_field_data
	WHERE parent_id = '46898'
```

```sql
======================= api reponses success ===================
SELECT
	q.api,
	q.qrid,
	DATE_FORMAT( FROM_UNIXTIME( q.sent ), '%Y-%m-%d %h:%m:%s' ) AS created_at,
	DATE_FORMAT( FROM_UNIXTIME( q.retrieved ), '%Y-%m-%d %h:%m:%s' ) AS updated_at,
	q.response_code,
	q.sent,
	q.retrieved
FROM
	quote_api_raw_responses AS q 
WHERE q.api = 'petsbest'
	AND q.sent BETWEEN UNIX_TIMESTAMP('2020/06/01 00:00:00') AND UNIX_TIMESTAMP('2020/06/30 23:59:59')
ORDER BY
	q.qrid ASC 
	LIMIT 10
```

```sql
======================= Calculate api reponses success ===================
SELECT
	count(*),
	SUM(q.sent) AS sent,
	SUM(q.retrieved) AS retr,
	SUM(q.retrieved) - SUM(q.sent) AS t
FROM
	quote_api_raw_responses AS q 
WHERE
	q.response_code = 200 
	AND q.api = 'spot'
	AND q.sent BETWEEN UNIX_TIMESTAMP('2020/06/01 00:00:00') AND UNIX_TIMESTAMP('2020/06/30 23:59:59')
```

```sql
======================= Every day click record ===================
SELECT 
	DATE_FORMAT(FROM_UNIXTIME(c.created), '%Y-%m-%d') AS click_created,
	count(*) AS test
FROM click AS c
WHERE insurer = '115' 
AND type = '23'
AND c.created BETWEEN UNIX_TIMESTAMP('2020-04-01 00:00:01') AND UNIX_TIMESTAMP('2020-04-30 23:59:59')
GROUP BY DATE_FORMAT(FROM_UNIXTIME(c.created), '%Y-%m-%d')
ORDER BY c.created ASC
```

```sql
======================= Every day get quote record ===================
SELECT
	DATE_FORMAT( FROM_UNIXTIME( q.created ), '%Y-%m-%d' ) AS created ,
	count(*) AS test
FROM
	quote_response AS r
	LEFT JOIN quote_request AS q ON q.id = r.quote_request 
WHERE
	insurer = '115' 
	AND q.created BETWEEN UNIX_TIMESTAMP( '2020-08-01 00:00:01' ) 
	AND UNIX_TIMESTAMP( '2020-08-13 23:59:59' ) 
GROUP BY
	DATE_FORMAT( FROM_UNIXTIME( q.created ), '%Y-%m-%d' ) 
ORDER BY
	q.created ASC
```

```sql
======================= How many new emails we are adding a month? ===================
SELECT
	q.email 
FROM
	quote_request AS q
	LEFT JOIN users_field_data AS u ON q.user__target_id = u.uid 
	WHERE u.`status` = 1
	AND u.created BETWEEN UNIX_TIMESTAMP( '2020-07-01 00:00:01' ) AND UNIX_TIMESTAMP('2020-07-31 23:59:59')
	GROUP BY q.email
```

```sql
======================= utms ===========================
SELECT
	utm.quote_id,
	q.email,
	q.last_name,
	q.first_name,
	q.pet_species,
	q.pet_breed,
	utm.utm_source,
	utm.utm_medium,
	utm.utm_campaign,
	utm.utm_content,
	utm.created_at,
	q.user_agent
FROM
	quote_utms AS utm
	LEFT JOIN quote_pageviews AS p ON utm.quote_pageview_id = p.id
	LEFT JOIN quote_request AS q ON utm.quote_id = q.id 
ORDER BY
	utm.quote_id DESC
```

```sql
======================= Review authentication ===========================
SELECT
	n.nid AS nid,
	insurer.nid AS insurer_id,
	type.field_verify_type_value AS verify_type,
	email.field_email_value AS email
FROM
	node_field_data AS n
	LEFT JOIN node__field_insurer AS nfi ON n.nid = nfi.entity_id
	LEFT JOIN node_field_data AS insurer ON nfi.field_insurer_target_id = insurer.nid
	LEFT JOIN node__field_archived AS arc ON n.nid = arc.entity_id 
	LEFT JOIN node__field_verify_type AS type ON n.nid = type.entity_id
	LEFT JOIN node__field_email AS email ON n.nid = email.entity_id
WHERE
	(
	n.type IN ( 'review' )) 
	AND ( n.`status` != '0' ) 
	AND insurer.nid = 121
	AND ((arc.field_archived_value = '0') OR ( arc.field_archived_value IS NULL )) 
	AND email.field_email_value IS NULL
```

```sql
======================= Quote insurer_sorting ===========================
SELECT
	q.id AS quote_id,
	DATE_FORMAT( FROM_UNIXTIME( q.created ), '%Y-%m-%d' ) AS created,
	SUBSTRING_INDEX( SUBSTRING_INDEX( q.insurer_sorting, ';', 1 ), ':',- 1 ) AS sort1,
	SUBSTRING_INDEX( SUBSTRING_INDEX( q.insurer_sorting, ';', 2 ), ':',- 1 ) AS sort2,
	SUBSTRING_INDEX( SUBSTRING_INDEX( q.insurer_sorting, ';', 3 ), ':',- 1 ) AS sort3,
	SUBSTRING_INDEX( SUBSTRING_INDEX( q.insurer_sorting, ';', 4 ), ':',- 1 ) AS sort4,
	SUBSTRING_INDEX( SUBSTRING_INDEX( q.insurer_sorting, ';', 5 ), ':',- 1 ) AS sort5,
	SUBSTRING_INDEX( SUBSTRING_INDEX( q.insurer_sorting, ';', 6 ), ':',- 1 ) AS sort6,
	SUBSTRING_INDEX( SUBSTRING_INDEX( q.insurer_sorting, ';', 7 ), ':',- 1 ) AS sort7,
	SUBSTRING_INDEX( SUBSTRING_INDEX( q.insurer_sorting, ';', 8 ), ':',- 1 ) AS sort8,
	SUBSTRING_INDEX( SUBSTRING_INDEX( q.insurer_sorting, ';', 9 ), ':',- 1 ) AS sort9,
	SUBSTRING_INDEX( SUBSTRING_INDEX( q.insurer_sorting, ';', 10 ), ':',- 1 ) AS sort10,
	SUBSTRING_INDEX( SUBSTRING_INDEX( q.insurer_sorting, ';', 11 ), ':',- 1 ) AS sort11,
	SUBSTRING_INDEX( SUBSTRING_INDEX( q.insurer_sorting, ';', 12 ), ':',- 1 ) AS sort12,
	SUBSTRING_INDEX( SUBSTRING_INDEX( q.insurer_sorting, ';', 13 ), ':',- 1 ) AS sort13,
	SUBSTRING_INDEX( SUBSTRING_INDEX( q.insurer_sorting, ';', 14 ), ':',- 1 ) AS sort14,
	SUBSTRING_INDEX( SUBSTRING_INDEX( q.insurer_sorting, ';', 15 ), ':',- 1 ) AS sort15,
	SUBSTRING_INDEX( SUBSTRING_INDEX( q.insurer_sorting, ';', 16 ), ':',- 1 ) AS sort16,
	SUBSTRING_INDEX( SUBSTRING_INDEX( q.insurer_sorting, ';', 17 ), ':',- 1 ) AS sort17,
	SUBSTRING_INDEX( SUBSTRING_INDEX( q.insurer_sorting, ';', 18 ), ':',- 1 ) AS sort18,
	SUBSTRING_INDEX( SUBSTRING_INDEX( q.insurer_sorting, ';', 19 ), ':',- 1 ) AS sort19,
	SUBSTRING_INDEX( SUBSTRING_INDEX( q.insurer_sorting, ';', 20 ), ':',- 1 ) AS sort20,
	SUBSTRING_INDEX( SUBSTRING_INDEX( q.insurer_sorting, ';', 21 ), ':',- 1 ) AS sort21,
	SUBSTRING_INDEX( SUBSTRING_INDEX( q.insurer_sorting, ';', 22 ), ':',- 1 ) AS sort22,
	SUBSTRING_INDEX( SUBSTRING_INDEX( q.insurer_sorting, ';', 23 ), ':',- 1 ) AS sort23,
	SUBSTRING_INDEX( SUBSTRING_INDEX( q.insurer_sorting, ';', 24 ), ':',- 1 ) AS sort24,
	SUBSTRING_INDEX( SUBSTRING_INDEX( q.insurer_sorting, ';', 25 ), ':',- 1 ) AS sort25,
	SUBSTRING_INDEX( SUBSTRING_INDEX( q.insurer_sorting, ';', 26 ), ':',- 1 ) AS sort26,
	SUBSTRING_INDEX( SUBSTRING_INDEX( q.insurer_sorting, ';', 27 ), ':',- 1 ) AS sort27,
	SUBSTRING_INDEX( SUBSTRING_INDEX( q.insurer_sorting, ';', 28 ), ':',- 1 ) AS sort28,
	SUBSTRING_INDEX( SUBSTRING_INDEX( q.insurer_sorting, ';', 29 ), ':',- 1 ) AS sort29,
	q.insurer_sorting
FROM
	quote_request AS q 
WHERE
	q.created BETWEEN UNIX_TIMESTAMP( '2021-03-01 00:00:01' ) 
	AND UNIX_TIMESTAMP( '2021-03-31 23:59:59' ) 
ORDER BY
	q.id DESC
```

```sql
======================= Top 20 activities region/location ===========================
SELECT
	count(SUBSTRING_INDEX( SUBSTRING_INDEX( c.location_data, ',', 2 ), ';',- 1 )) AS sort,
	SUBSTRING_INDEX( SUBSTRING_INDEX( c.location_data, ',', 2 ), ';',- 1 ) AS state 
FROM
	click AS c
WHERE
	c.created BETWEEN UNIX_TIMESTAMP( '2021-01-01 00:00:01' ) 
	AND UNIX_TIMESTAMP( '2021-01-31 23:59:59' )
AND c.status_reason IS NULL
GROUP BY state
ORDER BY
	sort DESC 
	LIMIT 20 OFFSET 0
```

```sql
======================= Leads-Facebook-UTMs ===========================
SELECT
	l.id,
	DATE_FORMAT( FROM_UNIXTIME( l.created ), '%Y-%m-%d' ) AS created,
	DATE_FORMAT( FROM_UNIXTIME( q.created ), '%Y-%m-%d' ) AS q_created,
	l.quote_request,
	q.email,
	q.pet_breed,
	l.insurer,
	u.utm_source,
	u.utm_medium,
	u.utm_content 
FROM
	leads l
	LEFT JOIN quote_request q ON l.quote_request = q.id 
	LEFT JOIN quote_utms u ON l.quote_request = u.quote_id
WHERE
	q.created BETWEEN 1617231600 AND 1619823599
ORDER BY
	l.id DESC
```

```sql
======================= Trupanion ranking ===========================
UPDATE node__field_rating
    LEFT JOIN node_field_data ON node_field_data.nid = node__field_rating.entity_id
    LEFT JOIN node__field_insurer ON node_field_data.nid = node__field_insurer.entity_id 
    SET field_rating_value = 10 
WHERE
    node__field_insurer.field_insurer_target_id = '121' 
    AND node__field_rating.field_rating_value = 5
```