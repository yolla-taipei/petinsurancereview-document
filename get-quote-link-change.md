#### compare dog insurance and compare cat insurance

1. [compare cat](https://www.petinsurancereview.com/cat-insurance)
    1. CMS
        1. [insurer_block_cat](https://www.petinsurancereview.com/admin/structure/views/view/insurers/edit/insurer_block_cat)
        1. [insurer_attachment_cat_canada](https://www.petinsurancereview.com/admin/structure/views/view/insurers/edit/insurer_attachment_cat_canada)
        1. [discount_insurer_block_cat](https://www.petinsurancereview.com/admin/structure/views/view/insurers/edit/discount_insurer_block_cat)
```console
Edit : FIELDS / Content: Get a quote button cat / Rewrite results
[Text]
<a href="{{ cat_redirect_link }}" target="_blank" class="btn btn-cta">Get a quote</a>
Change to : 
<a href="/get-quote" target="_blank" class="btn btn-cta">Get a quote</a>

Edit : FIELDS / Content: Get a quote button cat / No results behavior
[No results text]
{{ view_node | replace({ 'view' : 'View Details'}) | raw}}
Change to : 
<a href="/get-quote" target="_blank" class="btn btn-cta">Get a quote</a>
```

1. [compare dog](https://www.petinsurancereview.com/dog-insurance)
    1. CMS
        1. [insurer_block_dog](https://www.petinsurancereview.com/admin/structure/views/view/insurers/edit/insurer_block_dog)
        1. [insurers_attachment_dog_canada](https://www.petinsurancereview.com/admin/structure/views/view/insurers/edit/insurers_attachment_dog_canada)
        1. [discount_insurer_block_dog](https://www.petinsurancereview.com/admin/structure/views/view/insurers/edit/discount_insurer_block_dog)
```console
Edit : FIELDS / Content: Get a quote button dog / Rewrite results
[Text]
<a href="{{ field_dog_redirect_link }}" target="_blank" class="btn btn-cta">Get a quote</a>
Change to : 
<a href="/get-quote" target="_blank" class="btn btn-cta">Get a quote</a>

Edit : FIELDS / Content: Get a quote button cat / No results behavior
[No results text]
{{ view_node | replace({ 'view' : 'View Details'}) | raw}}
Change to : 
<a href="/get-quote" target="_blank" class="btn btn-cta">Get a quote</a>
```
