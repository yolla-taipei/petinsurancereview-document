```sql
SELECT
    qreq.email AS email,
    qreq.pet_name AS pet_name,
    qreq.pet_species AS pet_species,
    qreq.pet_breed AS pet_breed,
    qreq.pet_gender AS pet_gender,
    qreq.pet_born_month AS pet_born_month,
    qreq.pet_born_year AS pet_born_year,
    qreq.pet_spayed AS pet_spayed,
    qreq.first_name AS first_name,
    qreq.last_name AS last_name,
		DATE_FORMAT( FROM_UNIXTIME( qreq.created ), '%Y-%m-%d %h:%m:%s' ) AS created_at
FROM
    quote_request AS qreq
    LEFT JOIN users AS u ON qreq.user__target_id = u.uid
    LEFT JOIN user__roles AS ur ON u.uid = ur.entity_id 
WHERE
    qreq.email IS NOT NULL 
    AND qreq.first_name IS NOT NULL 
    AND qreq.last_name IS NOT NULL 
    AND qreq.pet_name IS NOT NULL 
    AND ur.roles_target_id = 'shadow'
		AND qreq.created BETWEEN UNIX_TIMESTAMP('2020-03-01 00:00:01') AND UNIX_TIMESTAMP('2020-03-31 23:59:59')
GROUP BY
    qreq.email,
    qreq.pet_name,
    qreq.pet_species,
    qreq.pet_breed,
    qreq.pet_gender,
    qreq.pet_born_month,
    qreq.pet_born_year,
    qreq.pet_spayed,
    qreq.first_name,
    qreq.last_name,
		qreq.created
ORDER BY
    qreq.created DESC 
```