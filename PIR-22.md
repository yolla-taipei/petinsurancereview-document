#### Site documentation ####

**_Pet Insurance Review (US) handbook_**

1. Drupal use PSR-4
1. pir-migrate 
   ``docroot/modules/custom/pir_migrate``
1. quote-api
    ``docroot/modules/custom/pir_p2_quotes/src/Plugin/QuoteAPI/{name}.php``
1. 報價為自動創建一個用戶，如果Email已存在就直接新增一筆報價
    1. 報價有效期限為30天
        ``docroot/modules/custom/pir_p2_quotes/src/Controller/viewQuotesAccess``
        ``docroot/modules/custom/pir_p2_quotes/src/QuoteRequestAccessControlHandler``
   1. Unlicensed states 
        ``docroot/modules/custom/pir_p2_quotes/src/Form/QuoteForm``
1. Rating weighted average calculation 
    ``docroot/modules/custom/pir_review_form/src/InsurerRating``
    1. 保險公司所有已發布評論四捨五入平均值
    1. weighted score also call combined score
    > flat score * (number of reviews for insurer / number of reviews for all insurers) * 100
    1. All reviews, reviews created in the last 90 days, 60 days, and 30 days. These are stored in the insurer_rating table.
1. Scheduled tasks (cron jobs) : via the Acquia cloud interface.
    1. **Every day at 05:00 UTC**, upload a CSV of leads to PetPlan’s FTPS server. This is
       implemented
       ``docroot/modules/custom/pir_lead/src/Controller/LeadsFTPController``
    1. **Every day at 08:30 UTC**, run the **clear-old-quotes** Drush command
    1. **Every hour at 6 minutes past the hour**, run Drupal cron (invoke all implementations of
       hook_cron)
       
       
**_Pet Insurance Review (US) CMS handbook_** 

1. Basic page can see [style-guide](https://www.petinsurancereview.com/style-guide)
1. Import of reviews [upload csv](https://www.petinsurancereview.com/admin/content/simple-csv-importer)
1. Exporting data from the system
    1. [Clicks](https://www.petinsurancereview.com/admin/clicks)
    1. [Quote Leads](https://www.petinsurancereview.com/admin/quotes-leads)
    1. [Canadian Quote Leads](https://www.petinsurancereview.com/admin/leads)
    
    