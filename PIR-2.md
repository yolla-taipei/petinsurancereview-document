#### User reviews
**_SQL_**

1. Filter not repeat
    1. user
        1. id、first_name、last_name、review_id、insurer_id
    1. pet
        1. pet_breed、pet_age
    1. role = 'shadow'.
````
SELECT
	count(*) AS email_count,
	ur.roles_target_id,
	ncnr.entity_id AS review_id,
	ces.last_comment_uid AS user_id,
	(SELECT mail FROM users_field_data WHERE uid = user_id) AS user_email,
	(SELECT field_nickname_value FROM node__field_nickname WHERE entity_id = user_id) AS user_name,
	(SELECT field_state_value FROM node__field_state WHERE entity_id = review_id) AS user_state,
	(SELECT field_insurer_target_id FROM node__field_insurer WHERE entity_id = review_id) AS insurer_id,
	(SELECT title AS insurer_name FROM node_field_data WHERE nid = insurer_id) AS insurer_title,
	(SELECT field_cat_breed_value FROM node__field_cat_breed WHERE entity_id = review_id) AS cat_breed,
	(SELECT field_dog_breed_value FROM node__field_dog_breed WHERE entity_id = review_id) AS dag_breed,
	(SELECT field_age_value FROM node__field_age WHERE entity_id = review_id) AS pet_age
FROM
	node__comment_node_review AS ncnr
	LEFT JOIN comment_entity_statistics AS ces ON ces.entity_id = ncnr.entity_id
	LEFT JOIN user__roles AS ur ON ces.last_comment_uid = ur.entity_id
WHERE
	ncnr.bundle = 'review' 
	AND ces.last_comment_uid IS NOT NULL
	AND ur.roles_target_id = 'shadow'
GROUP BY
	ncnr.entity_id,
	ces.last_comment_uid,
	ur.roles_target_id
````



#### User quotes
**_SQL_**

1. Filter not repeat
    1. user
        1. id、first_name、last_name、
    1. pet
        1. pet_name、pet_species、pet_breed、pet_gender、pet_born_month、pet_born_year、pet_spayed
    1. role = 'shadow'.
````
SELECT
	ur.roles_target_id AS role,
	qreq.user__target_id AS user_id,
	qreq.email AS user_email,
	qreq.first_name AS first_name,
	qreq.last_name AS last_name,
	qreq.pet_name AS pet_name,
	qreq.pet_species AS pet_species,
	qreq.pet_breed AS pet_breed,
	qreq.pet_gender AS pet_gender,
	qreq.pet_born_month AS pet_born_month,
	qreq.pet_born_year AS pet_born_year,
	qreq.pet_spayed AS pet_spayed 
FROM
	quote_request AS qreq
	LEFT JOIN users AS u ON qreq.user__target_id = u.uid
	LEFT JOIN user__roles AS ur ON u.uid = ur.entity_id 
WHERE
	qreq.email IS NOT NULL 
	AND qreq.first_name IS NOT NULL 
	AND qreq.last_name IS NOT NULL 
	AND qreq.pet_name IS NOT NULL 
	AND ur.roles_target_id = 'shadow' 
GROUP BY
	u.uid,
	qreq.email,
	qreq.pet_name,
	qreq.pet_species,
	qreq.pet_breed,
	qreq.pet_gender,
	qreq.pet_born_month,
	qreq.pet_born_year,
	qreq.pet_spayed,
	qreq.first_name,
	qreq.last_name 
ORDER BY
	u.uid DESC 
````