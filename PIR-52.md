#### Sync Dev and Stage database from Prod
1. Dump prod sql
2. Scp upload file
    1. petinsurancereview.prod@prod-pir:/home/petinsurancereview/test/backups/
    2. petinsurancereview.prod@prod-pir:/home/petinsurancereview/dev/backups/
3. Drop database 
4. Drush import database
    1. drush @petinsurancereview.dev ah-db-import /home/petinsurancereview/dev/backups/prod.sql.gz
    2. drush @petinsurancereview.test ah-db-import /home/petinsurancereview/test/backups/prod.sql.gz
